from Gaudi.Configuration import *
from Configurables import LHCbApp
LHCbApp().XMLSummary='summaryDaVinci_00013027_00000001_1.xml'
HistogramPersistencySvc().OutputFile = "DaVinci_00013027_00000001_1_Hist.root"
from DaVinci.Configuration import *
DaVinci().EvtMax=-1
DaVinci().HistogramFile = "DaVinci_00013027_00000001_1_Hist.root"
from Configurables import InputCopyStream
InputCopyStream().Output = "DATAFILE='PFN:00013027_00000001_1.bubdbsselection.dst' TYP='POOL_ROOTTREE' OPT='REC'"
LHCbApp().DDDBtag = "head-20110823"
LHCbApp().CondDBtag = "head-20110823"
ApplicationMgr().EvtMax = -1
def forceOptions():
 MessageSvc().Format = "%u % F%18W%S%7W%R%T %0W%M"
 MessageSvc().timeFormat = "%Y-%m-%d %H:%M:%S UTC"
appendPostConfigAction(forceOptions)
from Configurables import RecordStream
FileRecords = RecordStream("FileRecords")
FileRecords.Output = "DATAFILE='PFN:00013027_00000001_1.bubdbsselection.dst' TYP='POOL_ROOTTREE' OPT='REC'"
EventSelector().Input=[ 
"DATAFILE='PFN:~/cmtuser/DaVinci_v29r2/WG/CPConfig/scripts/00013026_00000001_1.BuBdBsSelection.dst' TYP='POOL_ROOTTREE' OPT='READ'",
"DATAFILE='PFN:~/cmtuser/DaVinci_v29r2/WG/CPConfig/scripts/00013026_00000001_2.BuBdBsSelection.dst' TYP='POOL_ROOTTREE' OPT='READ'"
]
