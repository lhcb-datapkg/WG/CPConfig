from Gaudi.Configuration import *
from Configurables import LHCbApp
LHCbApp().XMLSummary='summaryDaVinci_00013309_00000016_2.xml'
HistogramPersistencySvc().OutputFile = "DaVinci_00013309_00000016_2_Hist.root"
from DaVinci.Configuration import *
DaVinci().EvtMax=100
DaVinci().PrintFreq=1
DaVinci().HistogramFile = "DaVinci_00013309_00000016_2_Hist.root"
LHCbApp().DDDBtag = "head-20111102"
def forceOptions():
  MessageSvc().Format = "%u % F%18W%S%7W%R%T %0W%M"
  MessageSvc().timeFormat = "%Y-%m-%d %H:%M:%S UTC"
appendPostConfigAction(forceOptions)
DaVinci().TupleFile = "00013309_00000016_2.root"
EventSelector().Input=[ "DATAFILE='PFN:/tmp/gcowan/00013309_00000016_1.bubdbsselection.dst' TYP='POOL_ROOTTREE' OPT='READ'"];


FileCatalog().Catalogs= ["xmlcatalog_file:pool_xml_catalog.xml"]
