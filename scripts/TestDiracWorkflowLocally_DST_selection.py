from Gaudi.Configuration import *
from Configurables import LHCbApp
LHCbApp().XMLSummary='summaryDaVinci_00013026_00000001_1.xml'
HistogramPersistencySvc().OutputFile = "DaVinci_00013026_00000001_1_Hist.root"
from DaVinci.Configuration import *
DaVinci().EvtMax=1000
DaVinci().SkipEvents = 0
DaVinci().HistogramFile = "DaVinci_00013026_00000001_1_Hist.root"
LHCbApp().DDDBtag = "head-20111102"
LHCbApp().CondDBtag = "head-20111111"
ApplicationMgr().EvtMax = 1000
def forceOptions():
  MessageSvc().Format = "%u % F%18W%S%7W%R%T %0W%M"
  MessageSvc().timeFormat = "%Y-%m-%d %H:%M:%S UTC"
appendPostConfigAction(forceOptions)
from DSTWriters.__dev__.Configuration import SelDSTWriter
SelDSTWriter('SelDST', OutputFileSuffix = "00013026_00000001_1")
EventSelector().Input=[ 
 "DATAFILE='PFN:/castor/cern.ch/grid/lhcb/LHCb/Collision11/DIMUON.DST/00016987/0000/00016987_00000001_1.dimuon.dst' TYP='POOL_ROOTTREE' OPT='READ'" 
]
