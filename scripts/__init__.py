#!/usr/bin/env python
# =============================================================================
# @file __init__.py
# @author Albert Puig Navarro (albert.puig@cern.ch)
# @date 2011-07-01
# =============================================================================
"""WG/CPConfig ntuple production package"""

__author__ = "Greig Cowan (greig.cowan@cern.ch)"

def printUsage():
  string = """There are four main files:
1) BuBdBs_DST_selection.py - This is run on the DSTs of the DIMUON 
	stream and produces a single DST which *only* contains the stripped
	candidates for 5 decay channels (Bu2JpsiK, Bd2JpsiK*, Bs2JpsiPhi, 
	Bd2JpsiKS, Lb2JpsiL). No other selection is applied at this stage.
2) TestDiracWorkflowLocally_DST_selection.py - The above files will eventually
	be run by the Dirac production system on the grid. In order to test
	that this will work, it is necessary to set up a local environment
	which is as close to the Grid environment as possible and to use the
	same additional options as Dirac passes to DaVinci when running the
	job. This file should be very close (as of start of 2012) to the 
	additional options which Dirac uses when producing our stripped
	DSTs using the first file above.
3) TestDiracWorkflowLocally_DST_merging.py - Once the DSTs are made by Dirac
	they are merged into larger files before being saved on the Grid
	storage elements. As in the above case, it is necessary to test this
	merging step on some local files before asking Dirac to do it. This
	file should give very similar options for merging as are used on the
	grid. 
4) BuBdBs_NTUPLE_maker.py - This is run on the merged DST from the previous
	step and will make a single ntuple (using standard DecayTreeTuple tools)
	which can then be used for further offline processing. Once this is 
	produced, it should be checked that all necessary variables have been
	written and that the luminosity information is correct (often this
	has been broken in the past due to some problem with the DaVinci
	options in the merging step). Different versions of DaVinci can also
	mean that some variables disappear from the default DecayTreeTuple, 
	so be careful here.
5) TestDiracWorkflowLocally_ntuple.py - As above, these are similar to the
	options that Dirac needs to use to know about the ntuples that we make.

Plan of work:
a) Test that the files work locally by running them on a DST from the 
stripping test sample. 
	- Modify BuBdBs_DST_selection.py to point to the test DST.
	- SetupProject DaVinci --use-grid
	- gaudirun.py BuBdBs_DST_selection.py TestDiracWorkflowLocally_DST_selection.py | tee stdout
	- Check the stdout to make sure everything is OK. 
	- Should have a new DST available (call it DST_CP1 for now).
b) Repeat above step on a different DST to get DST_CP2.
c) Run the merging test on DST_CP1 and DST_CP2. 
	- Add the names of DST_CP1 and DST_CP2 to TestDiracWorkflowLocally_DST_merging.py
	- gaudirun.py TestDiracWorkflowLocally_DST_merging.py | tee stdout_merging
	- Check the stdout to make sure everything is OK. 
d) Run the ntuple production on the merged DST.
	- Modify TestDiracWorkflowLocally_ntuple.py to point to the merged DST.
	- gaudirun.py BuBdBs_NTUPLE_maker.py TestDiracWorkflowLocally_ntuple.py | tee stdout_ntuple
e) Once you are sure that everything is working, it is time to submit a
production to Dirac. Please contact myself and Antonio Falabella
<falabella@fe.infn.it> for further instructions.

Common problems:
	- Changes in DaVinci and Dirac lead to the options in Test*.py being slightly wrong.
	- When testing, try and use the correct DB tags which are given in the Test*.py files.
	- Luminosity information is not correctly merged.
"""
  print string

if __name__ == '__main__':
  print "="*30, "WG/CPConfig", "="*30
  print "Author:", __author__
  print
  printUsage()
  print
# EOF

