#!/usr/bin/env python
from Gaudi.Configuration import *
from Configurables import ( DaVinci, MessageSvc, FilterDesktop )
from PhysSelPython.Wrappers import ( AutomaticData, Selection,
                                     SelectionSequence, MultiSelectionSequence )
from DSTWriters.Configuration import SelDSTWriter, stripDSTElements, stripDSTStreamConf
from DSTWriters.microdstelements import *


from BetaSReaders import DSTSelections

#................................
#Bu

input_location_Bu = '/Event/AllStreams/Phys/BetaSBu2JpsiKDetachedLine'
input_data_Bu = AutomaticData( Location = input_location_Bu + '/Particles' ) 

dst_selection_Bu = 'Bu2JpsiKUnbiased'
offline_Bu = getattr( DSTSelections, dst_selection_Bu )()

dst_filter_Bu = FilterDesktop( 'OfflineFilter_Bu' )
dst_filter_Bu.Code = offline_Bu.cut() % offline_Bu.selection()
dst_filter_Bu.CloneFilteredParticles = True

DSTSelection_Bu = Selection( "Bu2JpsiK",
                          Algorithm = dst_filter_Bu,
                          RequiredSelections = [ input_data_Bu ] )

DSTSeq_Bu = SelectionSequence( "Bu2JpsiKSeq", TopSelection = DSTSelection_Bu )

output_location_Bu = '/Event/' + '/'.join(DSTSelection_Bu.outputLocation().split('/')[: -1])

# Bu prescaled
input_location_BuPrescaled = '/Event/AllStreams/Phys/BetaSBu2JpsiKPrescaledLine'
input_data_BuPrescaled = AutomaticData( Location = input_location_BuPrescaled + '/Particles' ) 

dst_filter_BuPrescaled = FilterDesktop( 'OfflineFilter_BuPrescaled' )
dst_filter_BuPrescaled.Code = offline_Bu.cut() % offline_Bu.selection()
dst_filter_BuPrescaled.CloneFilteredParticles = True

DSTSelection_BuPrescaled = Selection( "Bu2JpsiKPrescaled",
                          Algorithm = dst_filter_BuPrescaled,
                          RequiredSelections = [ input_data_BuPrescaled ] )

DSTSeq_BuPrescaled = SelectionSequence( "Bu2JpsiKPrescaledSeq", TopSelection = DSTSelection_BuPrescaled )
output_location_BuPrescaled = '/Event/' + '/'.join(DSTSelection_BuPrescaled.outputLocation().split('/')[: -1])

#................................
#Bd

input_location_Bd = '/Event/AllStreams/Phys/BetaSBd2JpsiKstarDetachedLine'		
input_data_Bd = AutomaticData( Location = input_location_Bd + '/Particles' )

dst_selection_Bd = 'Bd2JpsiKstarUnbiased'
offline_Bd = getattr( DSTSelections, dst_selection_Bd )()

dst_filter_Bd = FilterDesktop( 'OfflineFilter_Bd' )
dst_filter_Bd.Code = offline_Bd.cut() % offline_Bd.selection()
dst_filter_Bd.CloneFilteredParticles = True

DSTSelection_Bd = Selection( "Bd2JpsiKstar",
                          Algorithm = dst_filter_Bd,
                          RequiredSelections = [ input_data_Bd ] )

DSTSeq_Bd = SelectionSequence( "Bd2JpsiKstarSeq", TopSelection = DSTSelection_Bd )

output_location_Bd = '/Event/' + '/'.join(DSTSelection_Bd.outputLocation().split('/')[: -1])

# Bd Prescaled 
input_location_BdPrescaled = '/Event/AllStreams/Phys/BetaSBd2JpsiKstarPrescaledLine'		
input_data_BdPrescaled = AutomaticData( Location = input_location_BdPrescaled + '/Particles' )

dst_filter_BdPrescaled = FilterDesktop( 'OfflineFilter_BdPrescaled' )
dst_filter_BdPrescaled.Code = offline_Bd.cut() % offline_Bd.selection()
dst_filter_BdPrescaled.CloneFilteredParticles = True

DSTSelection_BdPrescaled = Selection( "Bd2JpsiKstarPrescaled",
                          Algorithm = dst_filter_BdPrescaled,
                          RequiredSelections = [ input_data_BdPrescaled ] )

DSTSeq_BdPrescaled = SelectionSequence( "Bd2JpsiKstarPrescaledSeq", TopSelection = DSTSelection_BdPrescaled )
output_location_BdPrescaled = '/Event/' + '/'.join(DSTSelection_BdPrescaled.outputLocation().split('/')[: -1])

#................................
#Bd2JpsiKs

input_location_Bd2JpsiKs = '/Event/AllStreams/Phys/BetaSBd2JpsiKsDetachedLine'		
input_data_Bd2JpsiKs = AutomaticData( Location = input_location_Bd2JpsiKs + '/Particles' )

dst_selection_Bd2JpsiKs = 'Bd2JpsiKsUnbiased'
offline_Bd2JpsiKs = getattr( DSTSelections, dst_selection_Bd2JpsiKs )()

dst_filter_Bd2JpsiKs = FilterDesktop( 'OfflineFilter_Bd2JpsiKs' )
dst_filter_Bd2JpsiKs.Code = offline_Bd2JpsiKs.cut() % offline_Bd2JpsiKs.selection()
dst_filter_Bd2JpsiKs.CloneFilteredParticles = True

DSTSelection_Bd2JpsiKs = Selection( "Bd2JpsiKs",
                          Algorithm = dst_filter_Bd2JpsiKs,
                          RequiredSelections = [ input_data_Bd2JpsiKs ] )

DSTSeq_Bd2JpsiKs = SelectionSequence( "Bd2JpsiKsSeq", TopSelection = DSTSelection_Bd2JpsiKs )

output_location_Bd2JpsiKs = '/Event/' + '/'.join(DSTSelection_Bd2JpsiKs.outputLocation().split('/')[: -1])

# Bd Prescaled 
input_location_Bd2JpsiKsPrescaled = '/Event/AllStreams/Phys/BetaSBd2JpsiKsPrescaledLine'		
input_data_Bd2JpsiKsPrescaled = AutomaticData( Location = input_location_Bd2JpsiKsPrescaled + '/Particles' )

dst_filter_Bd2JpsiKsPrescaled = FilterDesktop( 'OfflineFilter_Bd2JpsiKsPrescaled' )
dst_filter_Bd2JpsiKsPrescaled.Code = offline_Bd2JpsiKs.cut() % offline_Bd2JpsiKs.selection()
dst_filter_Bd2JpsiKsPrescaled.CloneFilteredParticles = True

DSTSelection_Bd2JpsiKsPrescaled = Selection( "Bd2JpsiKsPrescaled",
                          Algorithm = dst_filter_Bd2JpsiKsPrescaled,
                          RequiredSelections = [ input_data_Bd2JpsiKsPrescaled ] )

DSTSeq_Bd2JpsiKsPrescaled = SelectionSequence( "Bd2JpsiKsPrescaledSeq", TopSelection = DSTSelection_Bd2JpsiKsPrescaled )
output_location_Bd2JpsiKsPrescaled = '/Event/' + '/'.join(DSTSelection_Bd2JpsiKsPrescaled.outputLocation().split('/')[: -1])

#................................
#Bs

input_location_Bs = '/Event/AllStreams/Phys/BetaSBs2JpsiPhiDetachedLine'				
input_data_Bs = AutomaticData( Location = input_location_Bs + '/Particles' )

dst_selection_Bs = 'Bs2JpsiPhiUnbiased'
offline_Bs = getattr( DSTSelections, dst_selection_Bs )()

dst_filter_Bs = FilterDesktop( 'OfflineFilter_Bs' )
dst_filter_Bs.Code = offline_Bs.cut() % offline_Bs.selection()
dst_filter_Bs.CloneFilteredParticles = True

DSTSelection_Bs = Selection( "Bs2JpsiPhi",
                          Algorithm = dst_filter_Bs,
                          RequiredSelections = [ input_data_Bs ] )

DSTSeq_Bs = SelectionSequence( "Bs2JpsiPhiSeq", TopSelection = DSTSelection_Bs )

output_location_Bs = '/Event/' + '/'.join(DSTSelection_Bs.outputLocation().split('/')[: -1])

# Bs prescaled
input_location_BsPrescaled = '/Event/AllStreams/Phys/BetaSBs2JpsiPhiPrescaledLine'				
input_data_BsPrescaled = AutomaticData( Location = input_location_BsPrescaled + '/Particles' )

dst_filter_BsPrescaled = FilterDesktop( 'OfflineFilter_BsPrescaled' )
dst_filter_BsPrescaled.Code = offline_Bs.cut() % offline_Bs.selection()
dst_filter_BsPrescaled.CloneFilteredParticles = True

DSTSelection_BsPrescaled = Selection( "Bs2JpsiPhiPrescaled",
                          Algorithm = dst_filter_BsPrescaled,
                          RequiredSelections = [ input_data_BsPrescaled ] )

DSTSeq_BsPrescaled = SelectionSequence( "Bs2JpsiPhiPrescaledSeq", TopSelection = DSTSelection_BsPrescaled )
output_location_BsPrescaled = '/Event/' + '/'.join(DSTSelection_BsPrescaled.outputLocation().split('/')[: -1])


#................................
#Lambda_b

input_location_Lb = '/Event/AllStreams/Phys/BetaSLambdab2JpsiLambdaUnbiasedLine'				
input_data_Lb = AutomaticData( Location = input_location_Lb + '/Particles' )

dst_selection_Lb = 'Lambdab2JpsiLambdaUnbiased'
offline_Lb = getattr( DSTSelections, dst_selection_Lb )()

dst_filter_Lb = FilterDesktop( 'OfflineFilter_Lb' )
dst_filter_Lb.Code = offline_Lb.cut() % offline_Lb.selection()
dst_filter_Lb.CloneFilteredParticles = True

DSTSelection_Lb = Selection( "Lb2JpsiL",
                          Algorithm = dst_filter_Lb,
                          RequiredSelections = [ input_data_Lb ] )

DSTSeq_Lb = SelectionSequence( "Lb2JpsiLSeq", TopSelection = DSTSelection_Lb )
output_location_Lb = '/Event/' + '/'.join(DSTSelection_Lb.outputLocation().split('/')[: -1])

#..................................
#Bu+Bd+Bs+Lb in one selection

gaudiseq = MultiSelectionSequence("BuBdBsSelection", Sequences = [DSTSeq_Bu, DSTSeq_BuPrescaled
								, DSTSeq_Bd, DSTSeq_BdPrescaled
								, DSTSeq_Bd2JpsiKs, DSTSeq_Bd2JpsiKsPrescaled
								, DSTSeq_Bs, DSTSeq_BsPrescaled
								, DSTSeq_Lb
								] )

dstWriter = SelDSTWriter( 'SelDST',
                          WriteFSR = True,
                          OutputFileSuffix = "Offline",
                          SelectionSequences = [gaudiseq] ) #[ DSTSeq_Bu, DSTSeq_Bd,DSTSeq_Bs ] )

## More space for output
MessageSvc().Format = "% F%50W%S%15W%R%T %0W%M"

## DaVinci Configurable
dv = DaVinci (
  DataType   = '2011',
  Simulation = True,
  HistogramFile = 'histos.root',
  EvtMax = -1,
  Lumi = True
  )

dv.appendToMainSequence( [ dstWriter.sequence() ] )

from GaudiConf import IOHelper
IOHelper().inputFiles(
['/castor/cern.ch/grid/lhcb/MC/MC11a/ALLSTREAMS.DST/00013421/0000/00013421_00000011_1.allstreams.dst']
)
